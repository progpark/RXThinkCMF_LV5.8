<?php


namespace App\Services;

use App\Models\DepModel;

/**
 * 部门管理-服务类
 * @author 牧羊人
 * @since 2020/8/28
 * Class DepService
 * @package App\Services
 */
class DepService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/28
     * DepService constructor.
     */
    public function __construct()
    {
        $this->model = new DepModel();
    }
}
