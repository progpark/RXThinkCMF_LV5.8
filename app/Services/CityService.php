<?php


namespace App\Services;

use App\Models\CityModel;

/**
 * 城市管理-服务类
 * @author 牧羊人
 * @since 2020/8/30
 * Class CityService
 * @package App\Services
 */
class CityService extends BaseService
{
    /**
     * 构造函数
     * @author 牧羊人
     * @since 2020/8/30
     * CityService constructor.
     */
    public function __construct()
    {
        $this->model = new CityModel();
    }
}
